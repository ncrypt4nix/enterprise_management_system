from graphene import relay
from django.shortcuts import get_object_or_404


class NodeWithId(relay.Node):
    """Кастомная нода, для разрешения id"""
    @staticmethod
    def to_global_id(type, id):
        return id

    @staticmethod
    def get_node_from_global_id(info, global_id, only_type):
        original_qs = only_type._meta.model.objects.all()
        obj = get_object_or_404(only_type.get_queryset(original_qs, info), pk=global_id)
        if hasattr(only_type, 'connect_to_object'):
            obj = only_type.connect_to_object(obj, info)
        return obj
