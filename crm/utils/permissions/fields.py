import graphene

from django.forms import ModelForm
from graphene.types.objecttype import ObjectType


class PermissionCheckerMeta(ObjectType.__class__):
    def __new__(cls, name, bases, dct):
        assert dct.get('Meta'), f'Класс {name} должен определить класс Meta'
        if getattr(dct['Meta'], 'abstract', False):
            return super().__new__(cls, name, bases, dct)

        assert hasattr(dct['Meta'], 'form'), (
            'Подклассы класса PermissionCheckerAbstract должны определить поле form в классе Meta'
        )
        assert issubclass(dct['Meta'].form, ModelForm), (
            'Поле form должно быть наследником класса ModelForm'
        )
        form = dct['Meta'].form
        # Инициализируем поля формы для ObjectType
        dct['id'] = graphene.ID()
        for field in form._meta.fields:
            dct[field] = graphene.Boolean()
            dct[f'resolve_{field}'] = dct.get(
                f'resolve_{field}',
                lambda root, info, **kwargs: info.context.user.has_perm(
                    f'{form._meta.model._meta.app_label}.change_{form._meta.model._meta.model_name}'
                )
            )
            dct[f'clean_{field}'] = dct.get(
                f'clean_{field}',
                lambda root, info, **kwargs: info.context.user.has_perm(
                    f'{form._meta.model._meta.app_label}.add_{form._meta.model._meta.model_name}'
                )
            )
        _cls = super().__new__(cls, name, bases, dct)
        # Связываем форму и permission_checker
        form.permission_checker = _cls
        # Возвращаем поле формы, которое затирается при создании класса ObjectType
        # Игнорируя защиту ObjectType на изменения Meta
        _cls._meta.__dict__['form'] = form
        return _cls


class PermissionCheckerAbstract(ObjectType, metaclass=PermissionCheckerMeta):
    class Meta:
        abstract = True

    @classmethod
    def as_field(cls):
        return graphene.Field(cls)
