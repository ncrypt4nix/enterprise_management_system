from functools import wraps

from django.forms import ModelForm
from django.db.models.fields.related import ManyToManyField
from django.core.exceptions import ValidationError

from backend.crm.utils.permissions.fields import PermissionCheckerAbstract


def form_to_permission_form(cls):
    assert issubclass(cls, ModelForm), (
        'Форма, декорируемая form_to_permission_form, должна быть наследником класса ModelForm'
    )

    def check_permissions(func, field):
        verbose_name = cls._meta.model._meta.get_field(field).verbose_name

        @wraps(func)
        def wrap(self, *args, **kwargs):
            create_checker = getattr(self.permission_checker, f'clean_{field}')
            change_checker = getattr(self.permission_checker, f'resolve_{field}')

            if not self.instance.pk and not create_checker(self, self.info):
                raise ValidationError(f'Доступ на изменение поля: "{verbose_name}" запрещен!')
            if self.instance.pk:
                instance_field = getattr(self.instance, field, None)
                # ManyToManyField - частный случай. Queryset'ы нельзя сравнивать
                equality_test = (
                    self.cleaned_data[field] == instance_field
                    if not isinstance(self.instance._meta.get_field(field), ManyToManyField)
                    else set(self.cleaned_data[field]) == set(instance_field.all())
                )
                if not equality_test and not change_checker(self.instance, self.info):
                    raise ValidationError(f'Доступ на изменение поля: "{verbose_name}" запрещен!')
            return func(self, *args, **kwargs)
        return wrap

    class FormPermissionAbstract(cls):
        permission_checker = None

        def __init__(self, info=None, *args, **kwargs):
            """info установлен в None, потому что graphene зачем-то создает форму, при инициализации мутации"""
            self.info = info
            super().__init__(*args, **kwargs)

        def __new__(cls, *args, **kwargs):
            assert issubclass(cls.permission_checker, PermissionCheckerAbstract), (
                f'С формой {cls.__name__} не связан ни один PermissionChecker'
            )
            return super().__new__(cls)

        def is_valid(self):
            """Проверяет наличие атрибута info у формы"""
            if not self.info:
                raise ValidationError('Неизвестный пользователь')
            return super().is_valid()

    for field in FormPermissionAbstract._meta.fields:
        def construct_clean_field(field):
            """Замыкает значение field"""
            def clean_field(self):
                return self.cleaned_data[field]
            return clean_field

        func_name = f'clean_{field}'
        setattr(
            FormPermissionAbstract,
            func_name,
            check_permissions(
                getattr(
                    FormPermissionAbstract,
                    func_name,
                    construct_clean_field(field)
                ),
                field
            )
        )
    return FormPermissionAbstract
