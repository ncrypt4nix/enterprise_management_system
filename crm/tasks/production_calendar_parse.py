import requests
import datetime
from backend.salary.models import DayOff


class ProductionCalendarParse:
    CALENDAR_URL = 'http://xmlcalendar.ru/data/ru/{year}/calendar.json'

    def production_calendar_parse(self, year=datetime.datetime.now().year, future_only=True):
        date_start = datetime.date(year=year, month=1, day=1)
        today = datetime.date.today() if future_only else date_start
        exist_days = {
            i.date: i.id if not i.manual else None
            for i in DayOff.objects.filter(
                date__range=[
                    # Не трогаем прошедшие дни
                    date_start if today < date_start else today,
                    datetime.date(year=year + 1, month=1, day=1)
                ]
            )
        }
        to_create = []
        data = requests.get(self.CALENDAR_URL.format(year=year)).json()
        for month_data in data['months']:
            for day in map(int, filter(lambda i: i.isdigit(), month_data['days'].split(','))):
                day_off_date = datetime.date(year=year, month=month_data['month'], day=day)
                # Не трогаем прошедшие дни
                if day_off_date < today:
                    continue
                try:
                    exist_days.pop(day_off_date)
                except KeyError:
                    to_create.append(DayOff(date=day_off_date))
        DayOff.objects.bulk_create(to_create)
        DayOff.objects.filter(id__in=exist_days.values()).delete()
