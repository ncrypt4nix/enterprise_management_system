from django.contrib import admin
from django import forms
from backend.labels.models import Label, RelatedLabel, GroupLabel


@admin.register(GroupLabel)
class GroupLabelAdmin(admin.ModelAdmin):
    pass


class RelatedLabelFromInline(admin.TabularInline):
    model = RelatedLabel
    fk_name = 'to_label'


class RelatedLabelToInline(admin.TabularInline):
    model = RelatedLabel
    fk_name = 'from_label'


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    class LabelAdminForm(forms.ModelForm):
        class Meta:
            model = Label
            fields = '__all__'
            widgets = {
                'color': forms.TextInput(attrs={'type': 'color'}),
            }
    form = LabelAdminForm
    inlines = (RelatedLabelFromInline, RelatedLabelToInline)
