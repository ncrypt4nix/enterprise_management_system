"""
    Нужно продумать защиту от изменения группы метки,
    и добавить ограничения на установку в модели RelatedLabel меток из разных групп.
"""
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import ArrayField


class GroupLabel(models.Model):
    name = models.CharField('Имя группы', max_length=255)
    ident = models.CharField('Системный идентификатор', max_length=128, unique=True)
    content_type = models.ForeignKey('contenttypes.ContentType', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Группа меток'
        verbose_name_plural = 'Группы меток'
        unique_together = ('ident', 'content_type')
        indexes = (
           models.Index(fields=['ident', 'content_type']),
        )

    def __str__(self):
        return self.name


class RelatedLabel(models.Model):
    to_label = models.ForeignKey(
        'Label',
        on_delete=models.CASCADE,
        related_name='from_labels_related',
        verbose_name='В метку'
    )
    from_label = models.ForeignKey(
        'Label',
        on_delete=models.CASCADE,
        related_name='to_labels_related',
        verbose_name='Из метки'
    )
    name = models.CharField('Название связи', max_length=255)
    permissions = ArrayField(
        models.CharField('Строка разрешений', max_length=64),
        default=list,
        blank=True
    )

    class Meta:
        verbose_name = 'Связь меток'
        verbose_name_plural = 'Связи меток'

    def save(self, *args, **kwargs):
        if self.to_label.group_id != self.from_label.group_id:
            raise ValidationError('Невозможно установить связь между метками из разных групп')
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Label(models.Model):
    name = models.CharField('Имя метки', max_length=255)
    ident = models.CharField('Системный идентификатор', max_length=128, blank=True, null=True)
    color = models.CharField('Цвет', max_length=7, default='#FFFFFF', blank=True)
    group = models.ForeignKey(
        'GroupLabel',
        on_delete=models.CASCADE,
        verbose_name='Группа меток',
        related_name='labels'
    )
    to_labels = models.ManyToManyField(
        'self',
        verbose_name='Возможный выбор меток',
        related_name='from_labels',
        blank=True,
        through='RelatedLabel',
        through_fields=('from_label', 'to_label'),
    )

    class Meta:
        verbose_name = 'Метка'
        verbose_name_plural = 'Метки'
        unique_together = ('ident', 'group')
        indexes = (
           models.Index(fields=['ident', 'group']),
        )
        permissions = (
            ('view_any_related_labels', 'Может видеть все метки'),
        )

    def __str__(self):
        return self.name
