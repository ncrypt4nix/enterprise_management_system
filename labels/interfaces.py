from collections import defaultdict

from django.db import models

from backend.labels.models import Label


class LabelInterface(models.Model):
    labels = models.ManyToManyField(
        'labels.Label',
        blank=True,
        verbose_name='Метки'
    )
    __system_labels = None

    class Meta:
        abstract = True

    @classmethod
    def system_labels(cls):
        if not cls.__system_labels:
            assert hasattr(cls, 'MetaLabels') and hasattr(cls.MetaLabels, 'label_groups'), (
                'Чтобы использовать системные метки, необходимо их определить в MetaLabels.label_groups вашего класса'
            )
            cls.__system_labels = cls.LazyGroupLabelDict(cls.MetaLabels.label_groups)
        return cls.__system_labels

    class LazyGroupLabelDict(defaultdict):
        """
            Словарь, состоящий из системных меток, необходимых для бизнес-логики приложения.
            Ключом словаря являются идентификаторы групп меток,
            И возвращается словарь из обьектов Label, ключом которых также является идентификатор, но уже метки.
        """
        _label_groups = None

        def __init__(self, label_groups):
            self._label_groups = label_groups
            super().__init__(
                lambda key: {
                    label.ident: label
                    for label in Label.objects.filter(
                        ident__in=self._label_groups[key],
                        group__ident=key
                    )
                }
            )

        def __missing__(self, key):
            if key not in self._label_groups:
                raise KeyError
            value = self.default_factory(key)
            self[key] = value
            return value
