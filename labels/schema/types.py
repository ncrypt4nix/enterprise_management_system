import graphene
from graphene_django.debug import DjangoDebug
from graphene_django import DjangoObjectType

from django.conf import settings
from django.db.models import Q

from backend.labels.models import Label, RelatedLabel, GroupLabel
from backend.labels.filters import GroupLabelFilter
from backend.crm.utils.graphene_utils.types import NodeWithId
from backend.crm.utils.graphene_utils.connections import CountableConnectionBase


class LabelType(DjangoObjectType):
    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')

    class Meta:
        model = Label
        exclude = ('ident',)
        interfaces = (NodeWithId, )
        connection_class = CountableConnectionBase

    def resolve_to_labels_related(root, info, **kwargs):
        if info.context.user.has_perm('labels.view_any_related_labels'):
            return root.to_labels_related.all()
        label_perms = getattr(info.context.user, 'label_permissions', [])
        return root.to_labels_related.filter(Q(permissions__contained_by=label_perms) | Q(permissions__len=0))


class RelatedLabelType(DjangoObjectType):
    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')

    class Meta:
        model = RelatedLabel
        fields = '__all__'
        interfaces = (NodeWithId, )
        connection_class = CountableConnectionBase


class GroupLabelType(DjangoObjectType):
    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')

    class Meta:
        model = GroupLabel
        exclude = ('ident',)
        interfaces = (NodeWithId, )
        filterset_class = GroupLabelFilter
        connection_class = CountableConnectionBase
