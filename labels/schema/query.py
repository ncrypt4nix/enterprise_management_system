import graphene
from graphene_django.filter import DjangoFilterConnectionField
from backend.labels.schema.types import GroupLabelType
from backend.crm.utils.graphene_utils.types import NodeWithId


class Query(graphene.ObjectType):
    group_label = NodeWithId.Field(GroupLabelType)
    group_labels = DjangoFilterConnectionField(GroupLabelType)
