import django_filters
from django.contrib.contenttypes.models import ContentType

from backend.labels.models import GroupLabel


class GroupLabelFilter(django_filters.FilterSet):
    content_type = django_filters.ModelMultipleChoiceFilter(queryset=ContentType.objects.all())

    order_by = django_filters.OrderingFilter(
        fields=[
            'name'
        ]
    )

    class Meta:
        model = GroupLabel
        fields = [
            'content_type',
            'order_by'
        ]
