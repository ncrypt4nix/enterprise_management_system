from typing import Type, Union

from django.utils import timezone
from django.db.models import Model
from django.db.models.signals import m2m_changed
from django.db.models.fields.related import ManyToManyField, OneToOneField

from backend.project_logging.models import Journal
from backend.crm.current_user_middleware import get_current_user


def get_model_name(instance: Union[Type[Model], Model]) -> str:
    app_label = instance._meta.app_label
    object_name = instance._meta.object_name
    return f'{app_label}.{object_name}'


# Глобальная переменная для сохранения журналов между запросами
# TODO: 
history_models = {}


def journal_process_middleware(get_response):
    """
        Мидлвар, сохраняющий журналы, после изменения m2m полей
        Необходим, для корректной работы журналирования m2m
    """
    def middleware(request):
        response = get_response(request)
        # сохранение журнала имеет смысл только при изменении объектов
        if request.method in ['POST', 'PUT', 'DELETE', 'PATCH']:
            history_keys = list(history_models)
            for key in history_keys:
                try:
                    history_models.pop(key).save()
                except KeyError:  # Предупреждает неожиданные гонки
                    pass
        return response
    return middleware


class SngyModel(Model):
    """При наследовании от этой модели ведётся логирование всех изменений данных"""
    class NotExistField:
        """Заглушка для отсутствия поля, заменяющая None"""
        pass

    class Meta:
        abstract = True

    _initial_state = {}
    # Устанавливаем в None, чтобы не допустить ошибок при его использовании в классе
    _field_list = None
    _m2m_field_list = None
    __journal = None

    def __new__(cls, *args, **kwargs):
        # TODO: Проводить проверки при создании класса, а не экземпляра
        if not hasattr(cls, 'MetaJournal'):
            return super().__new__(cls)
        assert not all([getattr(cls.MetaJournal, 'fields', None), getattr(cls.MetaJournal, 'exlude', None)]), (
            'Запрещено использование fields и exclude вместе внутри MetaJournal для модели {cls}'
        )
        assert any([getattr(cls.MetaJournal, 'fields', None), getattr(cls.MetaJournal, 'exlude', None)]), (
            'Нужно объявить поле fields или exclude в MetaJournal для модели {cls}'
        )
        return super().__new__(cls)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._field_list = [field.name for field in self._meta.fields if not isinstance(field, OneToOneField)]
        self._m2m_field_list = [field.name for field in self._meta.get_fields() if isinstance(field, ManyToManyField)]
        # Фильтрация полей для журналирования
        if hasattr(self, 'MetaJournal'):
            if hasattr(self.MetaJournal, 'fields'):
                self._field_list = [field for field in self._field_list if field in self.MetaJournal.fields]
                self._m2m_field_list = [field for field in self._m2m_field_list if field in self.MetaJournal.fields]
            if hasattr(self.MetaJournal, 'exclude'):
                self._field_list = [field for field in self._field_list if field not in self.MetaJournal.exclude]
                self._m2m_field_list = [
                    field
                    for field in self._m2m_field_list
                    if field not in self.MetaJournal.exclude
                ]
        self._initial_state = self._curent_state
        if not self.pk:
            return
        # Не очень изящное решение. Следует коннектить поля в метклассе, собирая всех наследников
        # Таким способом создается коннект на каждый объект
        for field in self._m2m_field_list:
            m2m_changed.connect(self.__class__.m2m_changed_reciever, sender=getattr(self, field).through)

    @property
    def unique_str(self):
        return '.'.join([get_model_name(self), str(self.pk)])

    @property
    def _journal(self):
        if not self.__journal:
            self.__journal = Journal(
                model=get_model_name(self),
                instance_id=self.pk,
                user=get_current_user(),
                before={},
                after={},
                date=timezone.now()
            )
        return self.__journal

    def _recreate_journal(self, before={}, after={}):
        self.__journal = Journal.objects.create(
            model=get_model_name(self),
            # or необходим для журналирования удаления
            instance_id=self.pk or self._initial_state.get('id'),
            user=get_current_user(),
            before=before,
            after=after
        )

    @classmethod
    def m2m_changed_reciever(cls, sender, instance, action, model, **kwargs):
        """Обновление журнала изменений"""
        field_name = next(
            field
            for field in instance._m2m_field_list
            if getattr(instance, field).through == sender
        )
        # При изменении этого поля queryset должен быть в кеше, так что промахов быть не должно
        field_value = getattr(instance, field_name).all()
        if 'pre' in action and field_name not in instance._journal.before:
            instance._journal.before[field_name] = instance._journal.serialize_arg(field_value)
        if 'post' in action:
            instance._journal.after[field_name] = field_value
        # Сохраняем журнал в глобальную переменную истории, которая потом счищается по завершении request'а
        history_models[instance.unique_str] = instance._journal

    def get_initial_state(self):
        """Возвращает первоначальное состояние, преобразуя объекты в id"""
        return {
            key: getattr(value, 'id', value)
            for key, value in self._initial_state.items()
        }

    @property
    def _curent_state(self):
        """Текущее состояние объекта"""
        return {
            field: self._conver_field_value_to_history(field)
            for field in self._field_list
        }

    def _conver_field_value_to_history(self, name):
        """Геттер поля в его текущем состоянии"""
        # Если уже существует поле, то отдаем его
        # Оптимизация, для внешних ключей
        if self._state.fields_cache.get(name, None):
            return self._state.fields_cache[name]
        # Такая сложная структура используется для ускорения
        # Достаем поля, которых еще нет, но уже есть их id
        # Касается только внешних ключей
        return self.__dict__.get(
            f'{name}_id',
            self.__dict__.get(name, self.NotExistField())
        )

    def _expand_external_objects(self):
        """Разворачивает внешние объекты из id и устанавливает их в текущее и историческое состояние"""
        # Инициализируем класс первоначального состояния
        initial_object = self.__class__()
        for field in self._field_list:
            # Используем джанговскую магию ленивости чтобы достать объект из id
            field_value = getattr(self, field)

            if hasattr(self, f'{field}_id'):
                initial_field_value = self._initial_state.get(field)
                # Первоначальное значение уже может быть объектом, нужно вернуть его id, если это так
                initial_field_id = getattr(initial_field_value, 'id', initial_field_value)
                if getattr(self, f'{field}_id') == initial_field_id:
                    # Устанавливаем в field первоначального состояния сразу объект
                    setattr(initial_object, field, field_value)
                else:
                    # Иначе, достаем объект из БД
                    setattr(initial_object, f'{field}_id', initial_field_id)
                    # Повторяем магию ленивости джанги.
                    # Важный момент! Объект может только создаваться, и getattr может вызвать TypeError,
                    # если не указать дефолтное значение
                    getattr(initial_object, field, None)
            else:
                setattr(initial_object, field, self._initial_state.get(field))
        # Возвращаем initial_state, проинициализированным объектом
        self._initial_state = initial_object._curent_state

    def save(self, *args, **kwargs):
        self._expand_external_objects()
        is_created = not self.id
        Model.save(self, *args, **kwargs)
        _curent_state = self._curent_state
        if not is_created:
            self._recreate_journal(before=self._initial_state, after=_curent_state)
        # После метода save возможна дальнейшая работа с объектом
        self._initial_state = _curent_state

    def delete(self, *args, **kwargs):
        self._expand_external_objects()
        Model.delete(self, *args, **kwargs)
        self._recreate_journal(before=self._initial_state)
        # После метода delete возможна дальнейшая работа с объектом
        self._initial_state = self._curent_state
