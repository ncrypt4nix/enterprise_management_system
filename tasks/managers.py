from mptt.managers import TreeManager


class TaskManager(TreeManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)

    def full_queryset(self):
        return super().get_queryset()

    def opened(self):
        statuses = self.model.system_labels()['status']
        return self.get_queryset().exclude(
            labels__in=[
                statuses['closed'],
                statuses['is_done']
            ]
        )
