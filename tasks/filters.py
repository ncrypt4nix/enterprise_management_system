import datetime
import django_filters

from django import forms
from django.db.models import Q

from backend.tasks.models import Task
from backend.labels.models import Label
from backend.projects.models import Project
from backend.users.models import User
from backend.crm.utils.filter_fields.fields import NumberTypeIsFilter


class TaskFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains')
    level = NumberTypeIsFilter(field_class=forms.IntegerField, method='resolve_level')
    projects = django_filters.ModelMultipleChoiceFilter(queryset=Project.objects.all())
    author = django_filters.ModelMultipleChoiceFilter(queryset=User.objects.all())
    assignee = django_filters.ModelMultipleChoiceFilter(queryset=User.objects.all(), method='resolve_assignee')
    watchers = django_filters.ModelMultipleChoiceFilter(queryset=User.objects.all())
    priority = django_filters.MultipleChoiceFilter(choices=Task._meta.get_field('priority').choices)
    labels = django_filters.ModelMultipleChoiceFilter(queryset=Label.objects.all())

    created__gte = django_filters.DateFilter(field_name='created', lookup_expr='gte')
    created__lte = django_filters.DateFilter(field_name='created', lookup_expr='lte', method='filter_created_lte')
    deadline__gte = django_filters.DateFilter(field_name='deadline', lookup_expr='gte')
    deadline__lte = django_filters.DateFilter(field_name='deadline', lookup_expr='lte', method='filter_deadline_lte')

    only_me = django_filters.BooleanFilter(method='resolve_only_me')

    def lte_to_lt(self, queryset, name, value):
        """Сдвиг дня и смена lookup_expr, из-за несоответствия типов"""
        return queryset.filter(**{'__'.join([name, 'lt']): value + datetime.timedelta(days=1)})

    def filter_created_lte(self, queryset, name, value):
        return self.lte_to_lt(queryset, name, value)

    def filter_deadline_lte(self, queryset, name, value):
        return self.lte_to_lt(queryset, name, value)

    def resolve_level(self, queryset, name, value):
        return queryset.get_ancestors(include_self=True).filter(level=value)

    def resolve_assignee(self, queryset, name, value):
        if not value:  # Необходимо, для ограничения queryset'а при использовании ModelMultipleChoiceFilter
            return queryset
        return queryset.filter(Q(assignee__in=value) | Q(extra_assignee__in=value))

    def resolve_only_me(self, queryset, name, value):
        if value is not True:
            return queryset
        return queryset.filter(watchers=self.request.user)

    order_by = django_filters.OrderingFilter(
        fields=[
            'pk',
            'priority',
            'created',
            'deadline',
            'author',
            'assignee',
        ]
    )

    class Meta:
        model = Task
        fields = [
            'title',
            'projects',
            'author',
            'assignee',
            'watchers',
            'priority',
            'created__lte',
            'created__gte',
            'deadline__lte',
            'deadline__gte',
            'labels',
            'only_me',
            ####################################
            # WARNING!
            # Поля: level и order_by должны быть именно в таком порядке, соответственно.
            # И должны всегда быть последними в списке fields
            'level',
            'order_by'
            ####################################
        ]
