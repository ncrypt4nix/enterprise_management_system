import json
import datetime
from django.utils import timezone
from django.contrib.auth.models import Permission
from graphene_django.utils.testing import GraphQLTestCase


from backend.tasks import schema
from backend.users.factories import UserFactory
from backend.tasks.factories import TaskFactory
from backend.tasks.schema.types import TaskCounterType


class SchemaQueryTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = '/gql_test_api/'

    fixtures = [
        'document_kind.json',
        'labels.json',
    ]

    def setUp(self):
        self.user = UserFactory()
        can_view_task = Permission.objects.get(codename='view_task')
        self.user.user_permissions.set([can_view_task])

        self._client.force_login(self.user)
        return super().setUp()

    def test_task_counter(self):
        """Проверка запроса task_counter"""
        def request():
            response = self.query(
                '''
                    query {
                      taskCounter {
                        count
                        state
                      }
                    }
                ''',
            )
            # smoke test
            self.assertResponseNoErrors(response)
            return json.loads(response.content)

        # Тесты на проверку статуса выполнения задач
        counter = 0
        # Для теста создадим таск с рандомным пользователем
        TaskFactory()
        usually_deadline = timezone.now() + datetime.timedelta(days=5)

        # Есть незавершенные задачи
        task = TaskFactory(assignee=self.user, deadline=usually_deadline, is_private=False)
        task.labels.add(task.system_labels()['status']['assigned'])
        counter += 1
        response = request()
        self.assertEqual(counter, response['data']['taskCounter']['count'])
        self.assertEqual(TaskCounterType.State.HAS_UNFINISHED.name, response['data']['taskCounter']['state'])

        # Есть задачи, которые вернули на доработку
        task = TaskFactory(assignee=self.user, deadline=usually_deadline, is_private=False)
        task.labels.add(task.system_labels()['status']['in_rework'])
        counter += 1
        response = request()
        self.assertEqual(counter, response['data']['taskCounter']['count'])
        self.assertEqual(TaskCounterType.State.HAS_ON_REWORK.name, response['data']['taskCounter']['state'])

        # Есть задачи с дедлайном сегодня
        task = TaskFactory(assignee=self.user, deadline=timezone.now() + datetime.timedelta(hours=2), is_private=False)
        counter += 1
        response = request()
        self.assertEqual(counter, response['data']['taskCounter']['count'])
        self.assertEqual(TaskCounterType.State.IS_DEADLINE_TODAY.name, response['data']['taskCounter']['state'])

        # Есть задачи с просроченным дедлайном
        task = TaskFactory(assignee=self.user, deadline=timezone.now() - datetime.timedelta(days=1), is_private=False)
        counter += 1
        response = request()
        self.assertEqual(counter, response['data']['taskCounter']['count'])
        self.assertEqual(TaskCounterType.State.HAS_OVERDUE.name, response['data']['taskCounter']['state'])

        # Проверка закрытых задач
        task = TaskFactory(assignee=self.user, is_private=False)
        task.labels.add(task.system_labels()['status']['closed'])
        response = request()
        self.assertEqual(counter, response['data']['taskCounter']['count'])
