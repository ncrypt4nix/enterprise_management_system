from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from backend.tasks.models import Task, NotifyToTaskDeadline


@admin.register(Task)
class TaskAdmin(DraggableMPTTAdmin):
    list_display = (
        'tree_actions',
        'indented_title',
    )
    list_display_links = (
        'indented_title',
    )
    exclude = ('notification_logs', 'viewed_by_users', 'comments_as_solution')


@admin.register(NotifyToTaskDeadline)
class NotifyToTaskDeadlineAdmin(admin.ModelAdmin):
    pass
