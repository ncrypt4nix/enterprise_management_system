import graphene
from backend.crm.utils.permissions.fields import PermissionCheckerAbstract
from backend.tasks.forms import TaskForm
from backend.tasks.services.watchers import non_deleted_watchers


def _user_is_maintainer(current_user, task_instance):
    if current_user.has_perm('tasks.can_change_any_task'):
        return True
    task_maintainers = set(
        task_instance.author,
        task_instance.assignee, 
        *task_instance.extra_assignee.all()
    )
    return current_user in task_maintainers


class TaskPermissionChecker(PermissionCheckerAbstract):
    non_deleted_wathers = graphene.List(graphene.Int)

    class Meta:
        form = TaskForm

    def clean_is_deleted(form, info, **kwargs):
        return not form.cleaned_data['is_deleted']

    def resolve_is_deleted(root, info, **kwargs):
        return info.context.user == root.author

    def resolve_title(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_description(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_projects(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_parent(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_priority(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_deadline(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_assignee(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_extra_assignee(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_labels(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_document(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_watchers(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_comments_as_solution(root, info, **kwargs):
        return _user_is_maintainer(info.context.user, root)

    def resolve_non_deleted_wathers(root, info, **kwargs):
        return non_deleted_watchers(root)
