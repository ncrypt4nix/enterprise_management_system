import pytz
import random
import factory
from functools import partial
from backend.tasks.models import Task


fake = partial(factory.Faker, locale='ru_RU')


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    title = fake('text', max_nb_chars=10)
    priority = factory.LazyFunction(lambda: random.choice([i[0] for i in Task._meta.get_field('priority').choices]))

    deadline = fake('date_time', tzinfo=pytz.utc)

    author = factory.SubFactory('backend.users.factories.UserFactory')
    assignee = factory.SubFactory('backend.users.factories.UserFactory')

    is_private = fake('pybool')
    is_auto = fake('pybool')
