from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'backend.tasks'

    def ready(self):
        # TODO: Изменить на свое поле.
        # Конвертер изменяет form field по всему проекту, хоть и объявляется в текущей app'ке
        from graphene_django.forms.converter import convert_form_field
        from django_filters.fields import MultipleChoiceField

        @convert_form_field.register(MultipleChoiceField)
        def convert_multiple_choice_filter_to_list_field(field):
            from graphene import Int, List
            return List(Int, required=field.required)
