from backend.tasks.models import Task


def set_watchers(instance: Task):
    instance.watchers.add(*non_deleted_watchers(instance))


def non_deleted_watchers(instance: Task):
    return list(filter(None, (
        instance.assignee_id,
        instance.author_id,
        # Устанавливаем линейных руководителей для не приватных задач
        *[
            getattr(user, 'head_id', None)
            for user in (instance.assignee, instance.author)
            if not instance.is_private
        ],
    )))
