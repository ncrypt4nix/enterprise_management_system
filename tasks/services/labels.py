from backend.tasks.models import Task


def set_business_labels(instance: Task, initial_state: dict):
    label_set = {label.ident for label in instance.labels.all()}
    if not label_set and not instance.assignee:
        instance.labels.add(instance.system_labels()['status']['draft'])
    elif (not label_set or 'draft' in label_set) and instance.assignee:
        instance.labels.remove(instance.system_labels()['status']['draft'])
        instance.labels.add(instance.system_labels()['status']['assigned'])
    elif initial_state.get('assignee') != instance.assignee_id and 'in_work' in label_set:
        instance.labels.remove(instance.system_labels()['status']['in_work'])
        instance.labels.add(instance.system_labels()['status']['in_rework'])
