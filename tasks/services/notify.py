from typing import List

from backend.tasks.models import Task
from backend.notice.models import Notification


def notify_watchers(instance: Task, text: str, watchers: List[int] = None, context_user=None):
    if watchers is None:
        watchers = set(instance.watchers.all())
    watchers = set(filter(None, watchers))
    if context_user:
        watchers -= {context_user}
    Notification.objects.bulk_create([
        Notification(
            content_object=instance,
            purpose=watcher,
            created=context_user or instance.author,
            link=instance.get_absolute_url(),
            text=text,
        )
        for watcher in watchers
    ])


def remove_notifications(instance: Task):
    Notification.objects.filter(link=instance.get_absolute_url()).delete()
