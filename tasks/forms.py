from django.forms import ModelForm
from django.utils import timezone
from django.core.exceptions import ValidationError

from backend.tasks.models import Task
from backend.crm.utils.permissions.forms import form_to_permission_form


@form_to_permission_form
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = (
            'title',
            'description',
            'projects',
            'parent',
            'priority',
            'deadline',
            'assignee',
            'extra_assignee',
            'is_private',
            'watchers',
            'labels',
            'is_deleted',
            'document',
            'comments_as_solution',
        )

    def clean_deadline(self):
        deadline = self.cleaned_data['deadline']
        if deadline and self.instance.deadline != deadline and deadline < timezone.now():
            raise ValidationError('Дедлайн не может быть меньше текущего времени')
        return deadline
