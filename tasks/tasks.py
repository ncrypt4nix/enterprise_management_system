import inspect
import logging

from django.utils import timezone
from django.db.models import Q, F
from django_celery_beat.models import PeriodicTask

from backend.crm.tasks import production_only
from backend.synergycrm.celery import app
from backend.tasks.models import Task, NotifyToTaskDeadline
from backend.tasks.services.notify import notify_watchers

logger = logging.getLogger('production')


@production_only
@app.task(time_limit=5 * 60, max_retries=0)
def send_notify_about_deadline():
    now = timezone.now()
    func_name = f'{__name__}.{inspect.currentframe().f_code.co_name}'
    peridodic_tasks = PeriodicTask.objects.filter(
        task=func_name,
        interval__period='minutes'
    ).values_list('interval__every', flat=True)

    if not peridodic_tasks:
        logger.error(f'{func_name}: Периодическая задача не объявлена, но запущена!')
        return
    if 1 < len(peridodic_tasks):
        logger.error(f'{func_name}: Объявлено две или более периодических задач')
        return

    through_model = Task._meta.get_field('notification_logs').remote_field.through
    notification_logs = []
    for notify in NotifyToTaskDeadline.objects.all():
        task_qs = Task.objects.opened().filter(
            ~Q(notification_logs=notify)
            & Q(deadline__lte=now + notify.to_deadline)
            & Q(deadline__gte=F('created') + notify.to_deadline)
        )
        for task in task_qs:
            watchers = frozenset(task.extra_assignee.all()) | {task.assignee} | {task.author}
            notify_watchers(
                instance=task,
                watchers=watchers,
                text='До истечения времени задачи "{id}: {title}" ({author}) осталось: {to_deadline}'.format(
                    id=task.pk,
                    title=task.title,
                    to_deadline=notify.to_deadline,
                    author=f'({task.author})' if task.author else '',
                )
            )
            notification_logs.append(
                through_model(
                    notifytotaskdeadline_id=notify.id,
                    task_id=task.id
                )
            )
    through_model.objects.bulk_create(notification_logs)
