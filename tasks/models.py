import enum

from django.db import models
from django.utils import timezone
from mptt.models import MPTTModel, TreeForeignKey

from backend.synergycrm import sngy_model
from backend.labels.interfaces import LabelInterface
from backend.tasks.managers import TaskManager
from backend.documents.models import Document, DocumentKind


class NotifyToTaskDeadline(models.Model):
    """
        Используется для оповещения пользователей о истечении времени задачи.
        В дальнейшем планируется привязка к пользователям и выбор времени напоминания конкретным пользователям
        к конкретным задачам, через ManyToMany и кастомной through моделью
    """
    name = models.CharField('Название периода', max_length=255, blank=True, default='')
    to_deadline = models.DurationField('Период до напоминания', unique=True)

    class Meta:
        verbose_name = 'Оповещение до истечения срока задачи'
        verbose_name_plural = 'Оповещения до истечения срока задач'

    def save(self, *args, **kwargs):
        # TODO: Добавить защиту на изменение поля to_deadline
        if not self.pk:
            self.fill_past()

    def __str__(self):
        return f'{self.name}' or f'{self.to_deadline}'

    def __repr__(self):
        return f'{self.name}: {self.to_deadline}'

    def fill_past(self):
        now = timezone.now()
        through_model = self.task_set.through
        task_qs = Task.objects.opened().filter(
            ~models.Q(notification_logs=self)
            & models.Q(deadline__lte=now + self.to_deadline)
            & models.Q(deadline__gte=models.F('created') + self.to_deadline)
        )
        through_model.objects.bulk_create([
            through_model(
                notifytotaskdeadline_id=self.id,
                task_id=task.id
            )
            for task in task_qs
        ])


class Task(MPTTModel, sngy_model.SngyModel, LabelInterface):
    class Priority(enum.IntEnum):
        HIGH = 0
        MEDIUM = 1
        LOW = 2

    title = models.CharField('Название', max_length=255)
    description = models.TextField('Описание', blank=True, default='')
    projects = models.ManyToManyField('projects.Project', verbose_name='Проекты')
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    priority = models.PositiveSmallIntegerField(
        'Приоритет',
        choices=tuple((i.value, i.name) for i in Priority),
        default=Priority.MEDIUM.value
    )

    created = models.DateTimeField('Время создания', auto_now_add=True)
    deadline = models.DateTimeField('Крайний срок', blank=True, null=True)

    author = models.ForeignKey(
        'users.User',
        on_delete=models.PROTECT,
        related_name='created_tasks',
        verbose_name='Автор'
    )
    assignee = models.ForeignKey(
        'users.User',
        on_delete=models.PROTECT,
        related_name='tasks_in_process',
        verbose_name='Ответственный',
        blank=True,
        null=True,
    )
    extra_assignee = models.ManyToManyField(
        'users.User',
        blank=True,
        verbose_name='Исполнители',
    )
    watchers = models.ManyToManyField(
        'users.User',
        blank=True,
        related_name='observed_tasks',
        verbose_name='Наблюдатели'
    )

    is_private = models.BooleanField('Приватная задача', default=False)
    is_auto = models.BooleanField(default=False)
    document = models.OneToOneField(
        'documents.Document',
        verbose_name='Документ',
        on_delete=models.CASCADE,
        blank=True
    )
    is_deleted = models.BooleanField('Удалена', default=False)
    comments_as_solution = models.ManyToManyField(
        'comments.Comment',
        blank=True,
        verbose_name='Комментарии решения задачи'
    )
    viewed_by_users = models.ManyToManyField(
        'users.User',
        blank=True,
        related_name='viewed_tasks',
        verbose_name='Просмотрено пользователями'
    )
    notification_logs = models.ManyToManyField(NotifyToTaskDeadline)

    objects = TaskManager()

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        permissions = (
            ('can_view_private_tasks', 'Может просматривать приватные задачи'),
            ('can_change_any_task', 'Может изменять все задачи'),
        )
        indexes = (
           models.Index(fields=['created']),
           models.Index(fields=['deadline']),
        )

    class MetaLabels:
        label_permissions = {
            'is_assignee': lambda self, info: self.assignee == info.context.user,
            'is_author': lambda self, info: self.author == info.context.user
        }
        label_groups = {
            'status': {'draft', 'assigned', 'in_work', 'in_rework', 'is_done', 'closed'}
        }

    class MetaJournal:
        exlude = ['lft', 'rght', 'tree_id', 'level']

    def save(self, *args, **kwargs):
        # Инициализирует документ для поддержания связи OneToOne
        if not self.pk and not getattr(self, 'document', None):
            self.document = Document.objects.create(
                document_kind=DocumentKind.objects.get(eng_name='task'),
                created_by=self.author
            )
        super().save(*args, **kwargs)

    def comment_notify(self, comment):
        self.watchers.add(comment.user_id)
        return self.watchers.exclude(id=comment.user_id).values_list('id', flat=True)

    def comment_notify_filter(self, comment, notify_users):
        """Удаляет всех не наблюдателей из списка на оповещение о комментарии для приватной задачи"""
        return (
            notify_users - {i.id for i in self.watchers.all()}
            if self.is_private
            else set()
        )

    def get_absolute_url(self):
        return f'/task/{self.pk}'

    def __str__(self):
        return self.title
