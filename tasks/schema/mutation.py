import graphene
from graphene_django.forms.mutation import DjangoModelFormMutation

from backend.users.decorators import permission_required
from backend.tasks.forms import TaskForm
from backend.tasks.schema.types import TaskType
from backend.tasks.services.notify import notify_watchers, remove_notifications
from backend.tasks.services.labels import set_business_labels
from backend.tasks.services.watchers import set_watchers


class UpdateTaskMutation(DjangoModelFormMutation):
    task = graphene.Field(TaskType)

    class Meta:
        form_class = TaskForm

    @classmethod
    @permission_required(['tasks.add_task', 'tasks.change_task'])
    def perform_mutate(cls, form, info):
        if not form.instance.pk:
            form.instance.author = info.context.user
        created = not form.instance.pk
        # Данные из базы нужны для фильтрации пользователей, которых нужно оповестить
        # TODO: Добавить в журнал метод get_initinal_state,
        # который будет отдавать _initinal_state, без объектов, только id
        initial_state = form.instance.get_initial_state()
        initial_state['labels'] = set(form.instance.labels.all()) if not created else {}

        if form.cleaned_data['is_deleted']:
            remove_notifications(instance=form.instance)

        # Сохранение в БД
        obj = super().perform_mutate(form, info)
        task = obj.task
        set_watchers(task)
        # TODO: Перенести все эти оповещения в сервис
        if created:
            # Оповещения о создании задачи
            projs = task.projects.all()
            notify_text = 'Создана задача "{title}" по {projs_word} {projects}'.format(
                title=task.title,
                projs_word='проектам' if len(projs) > 1 else 'проекту',
                projects=','.join(map(str, projs)),
            )
            notify_watchers(
                instance=task,
                text=notify_text,
                context_user=info.context.user,
            )
        # Оповещение о назначении нового исполнителя
        if not created and form.instance.assignee_id != initial_state.get('assignee'):
            notify_watchers(
                instance=task,
                watchers={task.assignee},
                context_user=info.context.user,
                text=f'Вам назначена задача "{task.title}"',
            )
        # Оповещения о изменении меток задачи
        labels_in_request = set(form.cleaned_data['labels'])
        if labels_in_request != initial_state['labels'] and not created:
            notify_watchers(
                instance=task,
                context_user=info.context.user,
                text='Состояние задачи "{title}" {author} изменилось с <{_from}> на <{_to}>'.format(
                    title=task.title,
                    author=f'({task.author})' if task.author else '',
                    _from=', '.join(map(str, initial_state['labels'])),
                    _to=', '.join(map(str, labels_in_request)),
                ),
            )
        set_business_labels(task, initial_state)
        return obj

    @classmethod
    def get_form_kwargs(cls, root, info, **input):
        """Возвращает обратно поля instance, если они не указаны в запросе"""
        kwargs = super().get_form_kwargs(root, info, **input)
        kwargs['info'] = info
        instance = kwargs.get('instance')
        if not instance:
            return kwargs
        for field in instance._meta.fields:
            if field.name not in kwargs['data'] and field.name != 'id':
                kwargs['data'][field.name] = getattr(instance, field.name)
        return kwargs


class Mutation(graphene.ObjectType):
    update_task = UpdateTaskMutation.Field()
