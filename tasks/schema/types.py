import datetime
import graphene
from graphene_django.debug import DjangoDebug
from graphene_django import DjangoObjectType

from django.db.models import Q
from django.utils import timezone
from django.conf import settings

from backend.users.decorators import permission_required
from backend.tasks.models import Task
from backend.tasks.filters import TaskFilter
from backend.tasks.permissions import TaskPermissionChecker
from backend.crm.utils.graphene_utils.types import NodeWithId
from backend.crm.utils.graphene_utils.connections import CountableConnectionBase


class TaskType(DjangoObjectType):
    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')
    permissions = TaskPermissionChecker.as_field()

    class Meta:
        model = Task
        fields = '__all__'
        filterset_class = TaskFilter
        interfaces = (NodeWithId, )
        convert_choices_to_enum = False
        connection_class = CountableConnectionBase

    def resolve_permissions(root, info, **kwargs):
        return root

    @classmethod
    @permission_required('tasks.view_task')
    def get_queryset(cls, queryset, info):
        if info.context.user.has_perm('tasks.can_view_private_tasks'):
            return queryset
        return queryset.filter(Q(is_private=False) | Q(is_private=True, watchers=info.context.user)).distinct()

    def resolve_labels(root, info, **kwargs):
        # TODO: Перенести в интерфейс меток
        info.context.user.label_permissions = [
            perm
            for perm, func in root.MetaLabels.label_permissions.items()
            if func(root, info)
        ]
        return root.labels.all()

    @staticmethod
    def connect_to_object(obj: Task, info) -> Task:
        obj.viewed_by_users.add(info.context.user)
        return obj


class TaskCounterType(graphene.ObjectType):
    """Тип для вывода кол-ва назначенных задач и текущее состояние работы сотрудника над задачами"""
    class State(graphene.Enum):
        """Текущий статус выполнения задач"""
        HAS_NOT_TASKS = 0  # нет назначенных задач
        HAS_UNVIEWED = 1  # есть непросмотренные поставленные задачи
        HAS_OVERDUE = 2  # есть задачи с просроченным дедлайном
        IS_DEADLINE_TODAY = 3  # есть задачи с дедлайном сегодня
        HAS_ON_REWORK = 4  # есть задачи, которые вернули на доработку
        HAS_UNFINISHED = 5  # есть незавершенные задачи

    state = State()
    count = graphene.Int()

    def __init__(self, qs):
        self.system_labels = qs.model.system_labels()
        self.closed_labels = [
            self.system_labels['status']['is_done'],
            self.system_labels['status']['closed']
        ]
        self.queryset = qs.exclude(labels__in=self.closed_labels)

    def resolve_state(root, info):
        if root.queryset.filter(watchers=info.context.user).exclude(viewed_by_users=info.context.user).exists():
            return root.State.HAS_UNVIEWED
        if root.queryset.filter(deadline__lte=timezone.now()).exclude().exists():
            return root.State.HAS_OVERDUE
        today_range = (
            timezone.now().replace(hour=0, minute=0, second=0, microsecond=0),
            timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1)
        )
        if root.queryset.filter(deadline__range=today_range).exists():
            return root.State.IS_DEADLINE_TODAY
        if root.queryset.filter(labels=root.system_labels['status']['in_rework']).exists():
            return root.State.HAS_ON_REWORK
        if root.queryset.filter(
            labels__in=[
                root.system_labels['status']['in_work'],
                root.system_labels['status']['assigned']
            ]
        ).exists():
            return root.State.HAS_UNFINISHED
        return root.State.HAS_NOT_TASKS

    def resolve_count(root, info):
        return root.queryset.count()
