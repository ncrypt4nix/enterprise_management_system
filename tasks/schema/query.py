import graphene
from django.db.models import Q
from graphene_django.filter import DjangoFilterConnectionField
from backend.tasks.schema.types import TaskType, TaskCounterType
from backend.crm.utils.graphene_utils.types import NodeWithId


class Query(graphene.ObjectType):
    task = NodeWithId.Field(TaskType)
    tasks = DjangoFilterConnectionField(TaskType)
    get_tasks_slice = graphene.List(
        TaskType,
        search=graphene.String(),
        required_ids=graphene.List(graphene.Int),
        excluded_ids=graphene.List(graphene.Int),
    )
    task_counter = graphene.Field(TaskCounterType)

    def resolve_get_tasks_slice(root, info, search, excluded_ids=[], required_ids=[]):
        task_qs = TaskType._meta.model.objects.all()
        if search:
            search_query = Q(title__icontains=search)
            if search.isdigit():
                search_query |= Q(id=search)
            task_qs = task_qs.filter(search_query)
        task_qs = task_qs.exclude(id__in=excluded_ids)
        task_qs |= TaskType._meta.model.objects.filter(id__in=required_ids)
        # Заглушка для задачи: https://gitlab.sngy.ru/synergy/system/-/issues/2328
        task_qs = task_qs.exclude(is_private=True)
        return TaskType.get_queryset(task_qs, info)[:10]

    def resolve_task_counter(root, info):
        assigned_to_user = TaskType._meta.model.objects.filter(assignee=info.context.user)
        return TaskCounterType(TaskType.get_queryset(assigned_to_user, info))
